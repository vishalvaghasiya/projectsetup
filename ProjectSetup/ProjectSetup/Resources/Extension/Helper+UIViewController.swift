//
//  Helper+UIViewController.swift
//
//  Created by VISHAL on 09/11/20.
//  Copyright © 2020 VISHAL. All rights reserved.
//

import Foundation
import UIKit
import Photos
import Toast_Swift
extension UIViewController {
    func after(delay: Double,completion:@escaping()->()){
        DispatchQueue.main.asyncAfter(deadline: .now()+delay, execute: completion)
    }
    
    func main(completion:@escaping()->()){
        DispatchQueue.main.async {
            completion()
        }
    }
    
    func showToast(_ message: String, position: ToastPosition){
        VariableInfo.windows?.makeToast(message, position: position)
        //        self.view.makeToast(message, position: position)
    }
    
    func goBack(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func goRootView(){
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func showAlert(_ title: String?, message: String?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: { (action: UIAlertAction) in
        }))
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func showAlertAction(title:String, message:String,_ completion:@escaping ()->()) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { (action) in
            completion()
        }))
        alert.addAction(UIAlertAction(title: "No", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func isValidEmail(testStr:String, completion:@escaping()->()){
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        if emailTest.evaluate(with: testStr){
            completion()
        } else {
            showToast("please enter a valid email address.", position: .bottom)
        }
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func checkCameraPermission(completion:@escaping()->()){
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .denied:
            self.alertPromptToAllowCameraAccessViaSettings()
        case .restricted:
            self.showAlert("Restricted", message: "device owner must approve")
        case .authorized:
            completion()
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { success in
                if success {
                    completion()
                } else {
                    self.main {
                        self.showAlert("", message: "Permission denied")
                    }
                }
            }
        @unknown default:
            fatalError()
        }
    }
    
    func checkPhotoLibraryPermission(completion:@escaping()->()){
        switch PHPhotoLibrary.authorizationStatus() {
        case .denied, .restricted:
            self.alertPromptToAllowPhotoLibraryAccessViaSettings()
        case .authorized:
            completion()
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization({ (status) in
                if status == PHAuthorizationStatus.authorized{
                    completion()
                } else {
                    self.main {
                        self.showAlert("", message: "Permission denied")
                    }
                }
            })
        case .limited:
            completion()
            break
        @unknown default:
            fatalError()
        }
    }
    
    func alertPromptToAllowCameraAccessViaSettings() {
        let alert = UIAlertController(title: "\(AppInfo.appName) Would Like To Access the Camera", message: "Please grant permission to use the Camera so that you can customers benefit.", preferredStyle: .alert )
        alert.addAction(UIAlertAction(title: "Open Settings", style: .cancel) { alert in
            if let appSettingsURL = NSURL(string: UIApplication.openSettingsURLString) {
                UIApplication.shared.open(appSettingsURL as URL, options: [:], completionHandler: nil)
            }
        })
        main {
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func alertPromptToAllowPhotoLibraryAccessViaSettings() {
        let alert = UIAlertController(title: "\(AppInfo.appName)  Would Like To Access the Photo Library", message: "Please grant permission to use the Photo so that you can customers benefit.", preferredStyle: .alert )
        alert.addAction(UIAlertAction(title: "Open Settings", style: .cancel) { alert in
            if let appSettingsURL = NSURL(string: UIApplication.openSettingsURLString) {
                UIApplication.shared.open(appSettingsURL as URL, options: [:], completionHandler: nil)
            }
        })
        main{
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openURL(urlString:String){
        if let url = URL(string: urlString) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    func giveRating() {
        let rateAlert = UIAlertController.init(title: "Are you like this app?", message: "If you like this app, then you should have to give star rate and write a good review.", preferredStyle: .alert)
        rateAlert.addAction(UIAlertAction.init(title: "Later", style: .default, handler: nil))
        rateAlert.addAction(UIAlertAction.init(title: "Rate Now!", style: .default, handler: { (btn) in
            UIApplication.shared.open(URL.init(string: AccountInfo.rateLink)!, options: [:], completionHandler: nil)
        }))
        self.present(rateAlert, animated: true, completion: nil)
    }
    
    func shareApp() {
        let rateAlert = UIAlertController.init(title: "Are you like this app?", message: "If you like this app, then you should have to share app your friends.", preferredStyle: .alert)
        rateAlert.addAction(UIAlertAction.init(title: "Later", style: .default, handler: nil))
        rateAlert.addAction(UIAlertAction.init(title: "Share Now!", style: .default, handler: { (btn) in
            let imageShare = [ URL.init(string: AccountInfo.appURL)! ]
            let activityViewController = UIActivityViewController(activityItems: imageShare , applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            self.present(activityViewController, animated: true, completion: nil)
        }))
        self.present(rateAlert, animated: true, completion: nil)
    }
}
