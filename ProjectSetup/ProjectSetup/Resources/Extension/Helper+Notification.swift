//
//  Helper+Notification.swift
//
//  Created by VISHAL on 09/11/20.
//  Copyright © 2020 VISHAL. All rights reserved.
//

import Foundation
extension Notification.Name {
    static let SetPercentage = Notification.Name(rawValue: "SetPercentage")
    static let finishLoadingNativeAds = Notification.Name(rawValue: "finishLoadingNativeAds")
}
