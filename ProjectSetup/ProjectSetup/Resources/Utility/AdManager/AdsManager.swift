//
//  AdsManager.swift
//  Gallery
//
//  Created by Nexios on 28/10/20.
//  Copyright © 2020 Vishal. All rights reserved.
//

import Foundation
import UIKit
import GoogleMobileAds
import AppTrackingTransparency
import AdSupport
protocol UINativeAdDelegate {
    func adLoader(didFinish nativeAd: GADNativeAd)
}
protocol UIBannerAdDelegate {
    func adLoader(_ bannerView: GADBannerView)
}
internal enum AdsInfo { }
internal extension AdsInfo {
    
    static var isLiveAds: Bool {
        get {
            return (UserDefaults.standard.bool(forKey: "isLive"))
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "isLive")
            UserDefaults.standard.synchronize()
        }
    }
    
    static var noOfAds: Int {
        get {
            return (UserDefaults.standard.integer(forKey: "noOfAds"))
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "noOfAds")
            UserDefaults.standard.synchronize()
        }
    }
    
    static var showAdsCount: Int {
        get {
            return (UserDefaults.standard.integer(forKey: "showAdsCount"))
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "showAdsCount")
            UserDefaults.standard.synchronize()
        }
    }
    
    static var openApp_Ads: String {
        get {
            return (UserDefaults.standard.string(forKey: "openApp_Ads") ?? "")
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "openApp_Ads")
            UserDefaults.standard.synchronize()
        }
    }
    
    static var banner_Ads: String {
        get {
            return (UserDefaults.standard.string(forKey: "banner_Ads") ?? "")
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "banner_Ads")
            UserDefaults.standard.synchronize()
        }
    }
    
    static var interstitial_Ads: String {
        get {
            return (UserDefaults.standard.string(forKey: "interstitial_Ads") ?? "")
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "interstitial_Ads")
            UserDefaults.standard.synchronize()
        }
    }
    
    static var native_Ads: String {
        get {
            return (UserDefaults.standard.string(forKey: "native_Ads") ?? "")
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "native_Ads")
            UserDefaults.standard.synchronize()
        }
    }
}

class AdsManager: NSObject {
    static let shared = AdsManager()
    
    //MARK: - GADInterstitial
    var interstitial: GADInterstitialAd!
    var isShowAd = false {
        didSet {
            if isShowAd {
                if AdsInfo.isLiveAds {
                    if AdsInfo.noOfAds >= AdsInfo.showAdsCount {
                        self.presentInterstitial()
                    } else {
                        AdsInfo.noOfAds = AdsInfo.noOfAds + 1
                    }
                }
            }
        }
    }
    
    var openAdvt = false
    func createAndLoadInterstitial(){
        let request = GADRequest()
        GADInterstitialAd.load(withAdUnitID: AdsInfo.interstitial_Ads, request: request) { (ad, error) in
            if let error = error {
                print("Failed to load interstitial ad with error: \(error.localizedDescription)")
                return
            }
            self.interstitial = ad
            self.interstitial.fullScreenContentDelegate = self
        }
    }
    
    func presentInterstitial() {
        if let ad = interstitial {
            after(1) {
                KVNProgress.show(withStatus: "Ads Show")
                after(5){
                    KVNProgress.dismiss()
                    AdsInfo.noOfAds = 1
                    ad.present(fromRootViewController: UIApplication.shared.windows.first!.rootViewController!)
                }
                self.isShowAd = false
            }
        } else {
            print("Ad wasn't ready")
        }
    }
    
    var appOpenAd: GADAppOpenAd?
    func requestAppOpenAd() {
        appOpenAd = nil
        GADAppOpenAd.load(
            withAdUnitID: AdsInfo.openApp_Ads,
            request: GADRequest(),
            orientation: UIInterfaceOrientation.portrait,
            completionHandler: { [self] appOpenAd, error in
                if error != nil {
                    return
                }
                self.appOpenAd = appOpenAd
                self.appOpenAd?.fullScreenContentDelegate = self
            })
    }
    
    func tryToPresentAd() {
        if (appOpenAd != nil) {
            let rootController = UIApplication.shared.windows.first!.rootViewController
            appOpenAd!.present(fromRootViewController: rootController!)
        } else {
            requestAppOpenAd()
        }
    }
    
    //MARK: - Native Ad
    var adLoader: GADAdLoader!
    var nativeAd = GADNativeAd()
    var nativeAdView: GADNativeAdView!
    var delegate: UINativeAdDelegate?
    var bannerDelegate: UIBannerAdDelegate?
    
    func createNativeAds(_ vc: UIViewController) {
        let options = GADMultipleAdsAdLoaderOptions()
        self.adLoader = GADAdLoader.init(adUnitID: AdsInfo.native_Ads, rootViewController: vc, adTypes: [GADAdLoaderAdType.native], options: [options])
        self.adLoader.delegate = self
        self.adLoader.load(GADRequest.init())
    }
    
    //MARK: - Banner View
    func loadBannerAd(in bannerView: GADBannerView, _ vc: UIViewController) {
        if AdsInfo.isLiveAds{
            mainThread {
                bannerView.isHidden = false
            }
        }
        bannerView.adUnitID = AdsInfo.banner_Ads
        bannerView.rootViewController = vc
        
        let frame = { () -> CGRect in
            let rect = CGRect()
            mainThread {
                vc.view.frame.inset(by: vc.view.safeAreaInsets)
            }
            return rect
        }()
        let viewWidth = frame.size.width
        mainThread {
            bannerView.adSize = GADCurrentOrientationAnchoredAdaptiveBannerAdSizeWithWidth(viewWidth)
        }
        bannerView.load(GADRequest())
        bannerView.delegate = self
    }
    
    class func showAdvt() -> Bool {
        return true
    }
}

extension AdsManager: GADFullScreenContentDelegate {
    func ad(_ ad: GADFullScreenPresentingAd, didFailToPresentFullScreenContentWithError error: Error) {
        print("didFailToPresentFullScreenContentWithError")
        openAdvt = true
        self.requestAppOpenAd()
    }
    
    func adDidPresentFullScreenContent(_ ad: GADFullScreenPresentingAd) {
    }
    
    func adDidDismissFullScreenContent(_ ad: GADFullScreenPresentingAd) {
        self.requestAppOpenAd()
        createAndLoadInterstitial()
    }
}

extension AdsManager: GADBannerViewDelegate {
    func bannerViewDidReceiveAd(_ bannerView: GADBannerView) {
        bannerDelegate?.adLoader(bannerView)
    }
}

extension AdsManager: GADAdLoaderDelegate, GADNativeAdLoaderDelegate, GADNativeAdDelegate, GADVideoControllerDelegate {
    func adLoader(_ adLoader: GADAdLoader, didFailToReceiveAdWithError error: Error) {
    }
    
    func adLoader(_ adLoader: GADAdLoader, didReceive nativeAd: GADNativeAd) {
        self.nativeAd = nativeAd
        nativeAd.delegate = self
    }
    
    func videoControllerDidEndVideoPlayback(_ videoController: GADVideoController) {
    }
    
    func adLoaderDidFinishLoading(_ adLoader: GADAdLoader) {
        delegate?.adLoader(didFinish: self.nativeAd)
    }
}
