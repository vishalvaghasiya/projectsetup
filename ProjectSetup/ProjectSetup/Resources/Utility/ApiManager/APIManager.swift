//
//  APIManager.swift
//
//  Created by VISHAL VAGHASIYA on 30/06/19.
//  Copyright © 2021 VISHAL VAGHASIYA. All rights reserved.
//

import UIKit
import Alamofire

// For Live
let BASE_URL = "https://app.supremerehab.net/Api/v1/"
let IMAGE_BASE_URL = "https://app.supremerehab.net/"
let SOCKET_BASE_URL = "https://app.supremerehab.net:3434/"

//let BASE_URL = "https://testing.supremerehab.net/Api/v1/"
//let IMAGE_BASE_URL = "https://testing.supremerehab.net/"
//let SOCKET_BASE_URL = "https://testing.supremerehab.net:5454/"

//let BASE_URL = "http://192.168.29.3:1140/Api/v1/"
//let IMAGE_BASE_URL = "http://192.168.29.3:1140/"
//let SOCKET_BASE_URL = "https://testing.supremerehab.net:5454/"


//extension UIViewController {
//    func HttpRequest_Get_Auth(method: String,
//                              param: String,
//                              success: @escaping (_ responseObject: NSDictionary) -> Void,
//                              failure: @escaping (_ error: Error?) -> Void) {
//        var url = ""
//        if param != "" {
//            url = BASE_URL + method + "/" + param
//        } else {
//            url = BASE_URL + method
//        }
//        print("URL==>",url)
//        print("TOKEN==>",TOKEN)
//        let header: HTTPHeaders = [
//            "Authorization": "Bearer " + TOKEN
//        ]
//        
//        AF.request(url, method: .get, headers: header)
//            .responseJSON { (response) in
//                KVNProgress.dismiss()
//                SVProgressHUD.dismiss()
//                self.dismissGIFLoader()
//                switch response.result {
//                case .success:
//                    if let JSON = response.value as? NSDictionary {
//                        let flag = JSON.bool(forKey: "flag")
//                        if flag {
//                            if JSON.integer(forKey: "code") == 10 {
//                                success(JSON)
//                            } else if JSON.integer(forKey: "code") == 999{
//                                sessionExpired(for: "")
//                            } else {
//                                failure(nil)
//                                if JSON.string(forKey: "message")?.lowercased() == "Patient not Assign.".lowercased(){
//                                    return
//                                }
//                                self.showWaring(for: JSON.string(forKey: "message"))
//                            }
//                        } else {
//                            failure(nil)
//                            self.showWaring(for: JSON.string(forKey: "message"))
//                        }
//                    } else {
//                        failure(nil)
//                        self.showWaring(for: "Response can't convert in Dictionary")
//                    }
//                    break
//                case .failure(let error):
//                    if let data = response.data {
//                        print("Response Error:- \(url)\(NSString(data: data, encoding: String.Encoding.utf8.rawValue)!)")
//                        self.showWaring(for: error.localizedDescription)
//                    } else {
//                        self.showWaring(for: "Request failed!")
//                    }
//                    failure(error)
//                    break
//                }
//            }
//    }
//    
//    func HttpRequest_Get(method: String,
//                         param: String,
//                         success: @escaping (_ responseObject: NSDictionary) -> Void,
//                         failure: @escaping (_ error: Error?) -> Void) {
//        var url = ""
//        if param != "" {
//            url = BASE_URL + method + "/" + param
//        } else {
//            url = BASE_URL + method
//        }
//        print("URL",url)
//        AF.request(url, method: .get)
//            .responseJSON { (response) in
//                KVNProgress.dismiss()
//                SVProgressHUD.dismiss()
//                self.dismissGIFLoader()
//                switch response.result {
//                case .success:
//                    if let JSON = response.value as? NSDictionary {
//                        let flag = JSON.bool(forKey: "flag")
//                        if flag {
//                            if JSON.integer(forKey: "code") == 10 {
//                                success(JSON)
//                            } else if JSON.integer(forKey: "code") == 999{
//                                sessionExpired(for: "")
//                            } else {
//                                failure(nil)
//                                self.showWaring(for: JSON.string(forKey: "message"))
//                            }
//                        } else {
//                            failure(nil)
//                            self.showWaring(for: JSON.string(forKey: "message"))
//                        }
//                    } else {
//                        failure(nil)
//                        self.showWaring(for: "Response can't convert in Dictionary")
//                    }
//                    break
//                case .failure(let error):
//                    if let data = response.data {
//                        print("Response Error:- \(url)\(NSString(data: data, encoding: String.Encoding.utf8.rawValue)!)")
//                        self.showWaring(for: error.localizedDescription)
//                    } else {
//                        self.showWaring(for: "Request failed!")
//                    }
//                    failure(error)
//                    break
//                }
//            }
//    }
//    
//    func HttpRequest_POST_Auth(method: String,
//                               param: [String:Any],
//                               success: @escaping (_ responseObject: NSDictionary) -> Void,
//                               failure: @escaping (_ error: Error?) -> Void) {
//        
//        let url = BASE_URL + method
//        
//        let header: HTTPHeaders = [
//            "Authorization": "Bearer " + TOKEN
//        ]
//        print(url, TOKEN, param)
//        AF.request(url, method: .post, parameters: param, headers: header)
//            .responseJSON { (response) in
//                KVNProgress.dismiss()
//                SVProgressHUD.dismiss()
//                self.dismissGIFLoader()
//                switch response.result {
//                case .success:
//                    if let JSON = response.value as? NSDictionary {
//                        let flag = JSON.bool(forKey: "flag")
//                        if flag {
//                            if JSON.integer(forKey: "code") == 10 {
//                                success(JSON)
//                            } else if JSON.integer(forKey: "code") == 999{
//                                sessionExpired(for: "")
//                            } else {
//                                failure(nil)
//                                self.showWaring(for: JSON.string(forKey: "message"))
//                            }
//                        } else {
//                            failure(nil)
//                            self.showWaring(for: JSON.string(forKey: "message") as String)
//                        }
//                    } else {
//                        failure(nil)
//                        self.showWaring(for: "Response can't convert in Dictionary")
//                    }
//                    break
//                case .failure(let error):
//                    if let data = response.data {
//                        print("Response Error:- \(url)\(NSString(data: data, encoding: String.Encoding.utf8.rawValue)!)")
//                        self.showWaring(for: error.localizedDescription)
//                    } else {
//                        self.showWaring(for: "Request failed!")
//                    }
//                    failure(error)
//                    break
//                }
//            }
//    }
//    
//    func HttpRequest_POST(method: String,
//                          param: [String:Any],
//                          success: @escaping (_ responseObject: NSDictionary) -> Void,
//                          failure: @escaping (_ error: Error?) -> Void) {
//        
//        let url = BASE_URL + method
//        print(url, param)
//        AF.request(url, method: .post, parameters: param)
//            .responseJSON { (response) in
//                SVProgressHUD.dismiss()
//                KVNProgress.dismiss()
//                self.dismissGIFLoader()
//                switch response.result {
//                case .success:
//                    if let JSON = response.value as? NSDictionary {
//                        let flag = JSON.bool(forKey: "flag")
//                        if flag {
//                            if JSON.integer(forKey: "code") == 10 {
//                                success(JSON)
//                            } else if JSON.integer(forKey: "code") == 999{
//                                sessionExpired(for: "")
//                            } else if JSON.integer(forKey: "code") == 5 {
//                                let data = JSON.dictionaryy(forKey: "data") as NSDictionary
//                                TOKEN = data.string(forKey: "token")
//                                let vc = StoryboardScene.OnBoard.setPINVC.instantiate()
//                                self.navigationController?.pushViewController(vc, animated: true)
//                            } else {
//                                failure(nil)
//                                self.showWaring(for: JSON.string(forKey: "message"))
//                            }
//                        } else {
//                            failure(nil)
//                            self.showWaring(for: JSON.string(forKey: "message"))
//                        }
//                    } else {
//                        failure(nil)
//                        self.showWaring(for: "Response can't convert in Dictionary")
//                    }
//                    break
//                case .failure(let error):
//                    if let data = response.data {
//                        print("Response Error:- \(url)\(NSString(data: data, encoding: String.Encoding.utf8.rawValue)!)")
//                        self.showWaring(for: error.localizedDescription)
//                    } else {
//                        self.showWaring(for: "Request failed!")
//                    }
//                    failure(error)
//                    break
//                }
//            }
//    }
//    
//    func HttpRequest_UPLOAD_Auth(method: String,
//                                 param: [String:Any],
//                                 img:Data,
//                                 success: @escaping (_ responseObject: NSDictionary) -> Void,
//                                 failure: @escaping (_ error: Error?) -> Void) {
//        
//        let url = BASE_URL + method
//        
//        let header: HTTPHeaders = [
//            "Authorization": "Bearer " + TOKEN
//        ]
//        
//        AF.upload(multipartFormData: { (MultipartFormData) in
//            MultipartFormData.append(img, withName: "logo", fileName: "profile.jpeg", mimeType: "image/jpeg")
//            for (key,value) in param {
//                MultipartFormData.append((value as! String).data(using: .utf8)!, withName: key)
//            }
//        }, to: url, method: .post, headers: header).uploadProgress { (progress) in
//            print(progress.fractionCompleted)
//        } .responseJSON { response in
//            KVNProgress.dismiss()
//            SVProgressHUD.dismiss()
//            self.dismissGIFLoader()
//            switch response.result {
//            case .success:
//                if let JSON = response.value as? NSDictionary {
//                    let flag = JSON.bool(forKey: "flag")
//                    if flag {
//                        if JSON.integer(forKey: "code") == 10 {
//                            success(JSON)
//                        } else if JSON.integer(forKey: "code") == 999{
//                            sessionExpired(for: "")
//                        } else {
//                            failure(nil)
//                            self.showWaring(for: JSON.string(forKey: "message"))
//                        }
//                    } else {
//                        failure(nil)
//                        self.showWaring(for: JSON.string(forKey: "message"))
//                    }
//                } else {
//                    failure(nil)
//                    self.showWaring(for: "Response can't convert in Dictionary")
//                }
//                break
//            case .failure(let error):
//                if let data = response.data {
//                    print("Response Error:- \(url)\(NSString(data: data, encoding: String.Encoding.utf8.rawValue)!)")
//                    self.showWaring(for: error.localizedDescription)
//                } else {
//                    self.showWaring(for: "Request failed!")
//                }
//                failure(error)
//                break
//            }
//        }
//    }
//    
//    func HttpRequest_UPLOAD_Auth(method: String,
//                                 param: [String:Any],
//                                 img:Data,
//                                 keyName:String,
//                                 filename:String,
//                                 mimeType:String,
//                                 success: @escaping (_ responseObject: NSDictionary) -> Void,
//                                 failure: @escaping (_ error: Error?) -> Void) {
//        
//        let url = BASE_URL + method
//        
//        let header: HTTPHeaders = [
//            "Authorization": "Bearer " + TOKEN
//        ]
//        print(url, TOKEN)
//        print(param)
//        AF.upload(multipartFormData: { (MultipartFormData) in
//            MultipartFormData.append(img, withName: keyName, fileName: filename, mimeType: mimeType)
//            for (key,value) in param {
//                MultipartFormData.append((value as! String).data(using: .utf8)!, withName: key)
//            }
//        }, to: url, method: .post, headers: header).uploadProgress { (progress) in
//            print(progress.fractionCompleted)
//        } .responseJSON { response in
//            KVNProgress.dismiss()
//            SVProgressHUD.dismiss()
//            self.dismissGIFLoader()
//            switch response.result {
//            case .success:
//                if let JSON = response.value as? NSDictionary {
//                    let flag = JSON.bool(forKey: "flag")
//                    if flag {
//                        if JSON.integer(forKey: "code") == 10 {
//                            success(JSON)
//                        } else if JSON.integer(forKey: "code") == 999{
//                            sessionExpired(for: "")
//                        } else {
//                            failure(nil)
//                            self.showWaring(for: JSON.string(forKey: "message"))
//                        }
//                    } else {
//                        failure(nil)
//                        self.showWaring(for: JSON.string(forKey: "message"))
//                    }
//                } else {
//                    failure(nil)
//                    self.showWaring(for: "Response can't convert in Dictionary")
//                }
//                break
//            case .failure(let error):
//                if let data = response.data {
//                    print("Response Error:- \(url)\(NSString(data: data, encoding: String.Encoding.utf8.rawValue)!)")
//                    self.showWaring(for: error.localizedDescription)
//                } else {
//                    self.showWaring(for: "Request failed!")
//                }
//                failure(error)
//                break
//            }
//        }
//    }
//}
