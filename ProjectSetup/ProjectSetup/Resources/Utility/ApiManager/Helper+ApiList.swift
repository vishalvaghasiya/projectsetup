//
//  ApiList.swift

//  Created by VISHAL VAGHASIYA on 12/01/21.
//  Copyright © 2021 VISHAL VAGHASIYA. All rights reserved.
//

import Foundation
internal enum URL_Homecare { }
internal extension URL_Homecare {
    static var search = "search-homecare"
    static var singleDetails = "get_agency_single_details"
    static var getAllAgency = "get_all_agencies_homecare"
    static var getInsurance = "get_insurances"
    static var getLanguage = "get_languages"
}

internal enum URL_Homehealth { }
internal extension URL_Homehealth {
    static var search = "search-homehealth"
}

internal enum URL_Hospice { }
internal extension URL_Hospice {
    static var search = "search-hospice"
}

internal enum URL_Mobile { }
internal extension URL_Mobile {
    static var search = "search-mobilediagnostic"
}

internal enum URL_Pharmacy { }
internal extension URL_Pharmacy {
    static var search = "search-pharmacy"
}

internal enum URL_SeniorLiving { }
internal extension URL_SeniorLiving {
    static var search = "search-senior-living"
}

internal enum URL_Doctors { }
internal extension URL_Doctors {
    static var search = "search-doctors"
}

internal enum URL_DME { }
internal extension URL_DME {
    static var search = "search-durable-medical-equipments"
}

internal enum URL_SNF { }
internal extension URL_SNF {
    static var search = "search-skilled-nursing-facility"
}

internal enum URL_LTACH { }
internal extension URL_LTACH {
    static var search = "search-long-term-acute-care-hospitals"
}

internal enum URL_IPR { }
internal extension URL_IPR {
    static var search = "search-in-patient-rehab"
}

internal enum URL_SPE { }
internal extension URL_SPE {
    static var search = "search-senior-placement-experts"
}

internal enum URL_SRE { }
internal extension URL_SRE {
    static var search = "search-senior-relocation-experts"
}
