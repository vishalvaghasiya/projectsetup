//
//  KVNProgress.swift
//  HomeCare
//
//  Created by VISHAL VAGHASIYA on 19/01/21.
//  Copyright © 2021 VISHAL VAGHASIYA. All rights reserved.
//

import Foundation
import JGProgressHUD

var hud = JGProgressHUD()
class KVNProgress {
    class func show(){
        hud.textLabel.text = nil
        hud.detailTextLabel.text = nil
        hud.square = true
        hud.show(in: UIApplication.shared.windows.first!)
    }
    
    class func show(withStatus:String){
        hud.textLabel.text = withStatus
        hud.detailTextLabel.text = nil
        hud.square = false
        hud.show(in: UIApplication.shared.windows.first!)
    }
    
    class func show(_ progress:CGFloat ,status:String, on: UIView){
        hud.vibrancyEnabled = true
        hud.square = false
        
        hud.indicatorView = JGProgressHUDRingIndicatorView()
        
        let prog = Float(progress)*100.0
        hud.detailTextLabel.text = "\(String(format:"%.2f", prog))% Complete"
        hud.progress = Float(progress)
        hud.textLabel.text = status
        hud.show(in: on)
    }
    
    class func showError(withStatus:String){
        hud.square = false
        hud.textLabel.text = nil
        hud.detailTextLabel.text = withStatus
        hud.indicatorView = JGProgressHUDErrorIndicatorView.init()
        hud.show(in: UIApplication.shared.windows.first!)
        hud.dismiss(afterDelay: 3, animated: true) {
            hud = JGProgressHUD()
        }
    }

    class func showSuccess() {
        hud.textLabel.text = "Success"
        hud.detailTextLabel.text = nil
        hud.indicatorView = JGProgressHUDSuccessIndicatorView.init()
        hud.square = true
        hud.show(in: UIApplication.shared.windows.first!)
        hud.dismiss(afterDelay: 3, animated: true) {
            hud = JGProgressHUD()
        }
    }
    
    class func showSuccess(completion: (() -> Void)? = nil){
        hud.textLabel.text = "Success"
        hud.detailTextLabel.text = nil
        hud.indicatorView = JGProgressHUDSuccessIndicatorView.init()
        hud.square = true
        hud.show(in: UIApplication.shared.windows.first!)
        hud.dismiss(afterDelay: 3, animated: true) {
            hud = JGProgressHUD()
            completion?()
        }
    }
    
    class func showSuccess(withStatus:String?, completion: (() -> Void)? = nil){
        hud.square = false
        hud.textLabel.text = withStatus
        hud.detailTextLabel.text = nil
        hud.indicatorView = JGProgressHUDSuccessIndicatorView.init()
        hud.show(in: UIApplication.shared.windows.first!)
        hud.dismiss(afterDelay: 3, animated: true) {
            hud = JGProgressHUD()
            completion?()
        }
    }

    class func dismiss(completion: (() -> Void)? = nil){
        hud.dismiss(afterDelay: 1, animated: true) {
            hud = JGProgressHUD()
            completion?()
        }
    }
}
