//
//  CommonFuc.swift
//
//  Created by VISHAL on 09/11/20.
//  Copyright © 2020 VISHAL. All rights reserved.
//

import Foundation
import UIKit
import Reachability
import FirebaseAnalytics
import Alamofire
func RGBColor(r: CGFloat, g: CGFloat, b: CGFloat, a: CGFloat) -> UIColor {
    return UIColor(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: a)
}

func showErrorMessage(with text: String){
    KVNProgress.showError(withStatus: text)
}

func showSuccessMessage(with text: String){
    KVNProgress.showSuccess(withStatus: text)
}

func showProgress(with text: String){
    KVNProgress.show(withStatus: text == "" ? "Loading..." : text)
}

func dismissProgress(){
    KVNProgress.dismiss()
}

func checkInternet() -> Bool {
    do {
        let reachability = try Reachability()
        if reachability.connection == .unavailable {
            return false
        } else if reachability.connection == .cellular || reachability.connection == .wifi {
            return true
        } else {
            return false
        }
    } catch {
        return false
    }
}

func IsRandom(number:Int) -> Bool{
    let val = Int.random(in: 1..<1000)
    if val % number == 0 {
        return true
    }
    return false
}

func timeStringFrom(totalSeconds: Int) -> String {
    let seconds: Int = totalSeconds % 60
    let minutes: Int = (totalSeconds / 60) % 60
    let hours: Int = totalSeconds / 3600
    if hours == 0{
        return String(format: "%.2d:%.2d", minutes,seconds)
    }
    return String(format: "%.2d:%.2d:%.2d", hours,minutes,seconds)
}

func getHeaderString(str1:String, str2:String) -> NSMutableAttributedString {
    let navLabel = UILabel()
    let navTitle = NSMutableAttributedString(string: str1, attributes:[
                                                NSAttributedString.Key.foregroundColor: UIColor.white,
                                                NSAttributedString.Key.font: UIFont(name: "Roboto-Bold", size: 22)!])
    
    navTitle.append(NSMutableAttributedString(string: " " + str2, attributes:[
                                                NSAttributedString.Key.font: UIFont(name: "Roboto-Medium", size: 22)!,
                                                NSAttributedString.Key.foregroundColor: UIColor.white]))
    
    navLabel.attributedText = navTitle
    return navTitle
}

func after(_ delay:Double, closure:@escaping ()->()){
    DispatchQueue.main.asyncAfter(deadline: .now() + delay, execute: closure)
}

// Hide show Animation
func setView(view: UIView, hidden: Bool, closure:@escaping ()->()) {
    if hidden {
        UIView.animate(withDuration: 0.5, animations: {
            view.alpha = 0
        }) { (finished) in
            view.isHidden = finished
            closure()
        }
    } else {
        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            view.isHidden = hidden
            view.alpha = 1
            closure()
        })
    }
}

//func randomShowAds() -> Bool {
//    if UserDefaults.standard.value(forKey: "isAds") == nil {
//        UserDefaults.standard.set(1, forKey: "isAds")
//        UserDefaults.standard.synchronize()
//        return false
//    } else if UserDefaults.standard.value(forKey: "isAds") != nil {
//        var value = UserDefaults.standard.value(forKey: "isAds") as! Int
//        if value == 2 {
//            UserDefaults.standard.set(1, forKey: "isAds")
//            UserDefaults.standard.synchronize()
//            return true
//        } else {
//            value += 1
//            UserDefaults.standard.set(value, forKey: "isAds")
//            UserDefaults.standard.synchronize()
//            if value > 2 {
//                UserDefaults.standard.set(1, forKey: "isAds")
//                UserDefaults.standard.synchronize()
//                return true
//            } else {
//                return false
//            }
//        }
//    } else {
//        return false
//    }
//}

func addEvents(_ name: String, param: [String:Any]){
    Analytics.logEvent(name, parameters: param)
}

func isUpdateAvailable(callback: @escaping (Bool)->Void) {
    let bundleId = Bundle.main.infoDictionary!["CFBundleIdentifier"] as! String
    if checkInternet(){
        AF.request("https://itunes.apple.com/lookup?bundleId=\(bundleId)", method: .get, parameters: [:])
            .responseJSON { (response) in
                switch response.result {
                case .success:
                    if let json = response.value as? NSDictionary, let results = json["results"] as? NSArray, let entry = results.firstObject as? NSDictionary, let versionStore = entry["version"] as? String, let versionLocal = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
                        let arrayStore = versionStore.split(separator: ".")
                        let arrayLocal = versionLocal.split(separator: ".")
                        
                        if arrayLocal.count != arrayStore.count {
                            callback(true) // different versioning system
                        }
                        
                        // check each segment of the version
                        for (key, value) in arrayLocal.enumerated() {
                            if Int(value)! < Int(arrayStore[key])! {
                                callback(true)
                            }
                        }
                    }
                    callback(false)
                    break
                case .failure( _):
                    callback(false)
                    break
                }
            }
    }
    else{
        showErrorMessage(with: "Please check internet connection and retry.")
    }
}
