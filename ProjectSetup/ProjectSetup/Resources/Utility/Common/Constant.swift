//
//  Constant.swift
//
//  Created by VISHAL on 09/11/20.
//  Copyright © 2020 VISHAL. All rights reserved.
//

import Foundation
import UIKit

internal enum AppInfo { }
internal extension AppInfo {
    static let appName = "App name"
    static let appID = ""
}

internal enum AccountInfo { }
internal extension AccountInfo {
    static let rateLink = "https://itunes.apple.com/app/id\(AppInfo.appID)?mt=8&action=write-review"
    static let appURL = "https://itunes.apple.com/app/id\(AppInfo.appID)?mt=8"
    static let privacyPolicy = "https://sites.google.com/view/privacypolicycenter/privacy-policy"
    static let termsandCondition = "https://sites.google.com/view/privacypolicycenter/terms-conditions"
    static let contactUS = "https://sites.google.com/view/privacypolicycenter/contact-us"
    static let email = "vishalvaghasiya.ios@gmail.com"
}

internal enum UserDefaultInfo { }
internal extension UserDefaultInfo {
    static var token: String {
        get {
            return (UserDefaults.standard.string(forKey: "Token") ?? "123")
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "Token")
            UserDefaults.standard.synchronize()
        }
    }
}

internal enum VariableInfo { }
internal extension VariableInfo {
    static let userDefault = UserDefaults.standard
    static let appDelegate = (UIApplication.shared.delegate as! AppDelegate)
    static let windows = UIApplication.shared.windows.first
}

internal enum MessageInfo { }
internal extension MessageInfo {
    static let SERVERDOWN_MSG = "Server down, Please try after sometime!"
}

internal enum KeyInfo { }
internal extension KeyInfo {
    static let KEY_TEST = ""
}

var appDelegate:AppDelegate = (UIApplication.shared.delegate as! AppDelegate)

/*
 
 {
 // MARK: - OUTLET
 
 // MARK: - PROPERTY
 
 // MARK: - LIFE CYCLE
 override func viewDidLoad() {
 super.viewDidLoad()
 
 }
 
 // MARK: - UI SETUP
 
 // MARK: - BUTTON CLICK
 
 // MARK: - OTHER
 
 // MARK: - DELEGATE
 }
 
 */
