//
//  ImagePicker.swift
//  HomeCare
//
//  Created by VISHAL VAGHASIYA on 31/07/21.
//  Copyright © 2021 VISHAL VAGHASIYA. All rights reserved.
//

/*
 For Use
 let imagePicker:ImagePicker?
 
 // Did Load
 self.imagePicker = ImagePicker(presentationController: self, delegate: self)
 
 extension ProfileCaseManager: ImagePickerDelegate {
     func didSelect(image: UIImage?) {
        
     }
 }
 
 */

import UIKit
import Photos
public protocol ImagePickerDelegate: AnyObject {
    func didSelect(image: UIImage?)
}
open class ImagePicker: NSObject {
    
    private let pickerController: UIImagePickerController
    private weak var presentationController: UIViewController?
    private weak var delegate: ImagePickerDelegate?
    
    public init(presentationController: UIViewController, delegate: ImagePickerDelegate) {
        self.pickerController = UIImagePickerController()
        
        super.init()
        
        self.presentationController = presentationController
        self.delegate = delegate
        
        self.pickerController.delegate = self
        self.pickerController.allowsEditing = true
        self.pickerController.mediaTypes = ["public.image"]
    }
    
    private func action(for type: UIImagePickerController.SourceType, title: String) -> UIAlertAction? {
        guard UIImagePickerController.isSourceTypeAvailable(type) else {
            if type == .camera {
                checkCameraPermission { }
            } else if type == .photoLibrary {
                checkPhotoLibraryPermission { }
            } else if type == .savedPhotosAlbum {
                checkPhotoLibraryPermission { }
            }
            return nil
        }
        
        return UIAlertAction(title: title, style: .default) { [unowned self] _ in
            if type == .camera {
                checkCameraPermission {
                    DispatchQueue.main.async {
                        self.pickerController.sourceType = type
                        self.pickerController.modalPresentationStyle = .fullScreen
                        self.presentationController?.present(self.pickerController, animated: true)
                    }
                }
            } else {
                checkPhotoLibraryPermission {
                    DispatchQueue.main.async {
                        self.pickerController.sourceType = type
                        self.pickerController.modalPresentationStyle = .fullScreen
                        self.presentationController?.present(self.pickerController, animated: true)
                    }
                }
            }
        }
    }
    
    public func present(from sourceView: UIView) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        if let action = self.action(for: .camera, title: "Take photo") {
            alertController.addAction(action)
        }
        if let action = self.action(for: .savedPhotosAlbum, title: "Camera roll") {
            alertController.addAction(action)
        }
        
        if let action = self.action(for: .photoLibrary, title: "Photo library") {
            alertController.addAction(action)
        }
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            alertController.popoverPresentationController?.sourceView = sourceView
            alertController.popoverPresentationController?.sourceRect = sourceView.bounds
            alertController.popoverPresentationController?.permittedArrowDirections = [.down, .up]
        }
        self.presentationController?.present(alertController, animated: true)
    }
    
    private func pickerController(_ controller: UIImagePickerController, didSelect image: UIImage?) {
        controller.dismiss(animated: true, completion: nil)
        self.delegate?.didSelect(image: image)
    }
    
    private func checkCameraPermission(completion:@escaping()->()){
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .denied:
            self.alertPromptToAllowCameraAccessViaSettings()
        case .restricted: break
        //"device owner must approve"
        case .authorized:
            completion()
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { success in
                if success {
                    completion()
                } else {
                    DispatchQueue.main.async {
                        //"Permission denied"
                    }
                }
            }
        @unknown default:
            fatalError()
        }
    }
    
    private func checkPhotoLibraryPermission(completion:@escaping()->()){
        switch PHPhotoLibrary.authorizationStatus() {
        case .denied, .restricted:
            self.alertPromptToAllowPhotoLibraryAccessViaSettings()
        case .authorized:
            completion()
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization({ (status) in
                if status == PHAuthorizationStatus.authorized{
                    completion()
                } else {
                    DispatchQueue.main.async {
                        //"Permission denied"
                    }
                }
            })
        case .limited:
            break
        @unknown default:
            fatalError()
        }
    }
    
    private func alertPromptToAllowCameraAccessViaSettings() {
        let alert = UIAlertController(title: "\(AppInfo.appName)  Would Like To Access the Camera", message: "Please grant permission to use the Camera so that you can customer benefit.", preferredStyle: .alert )
        alert.addAction(UIAlertAction(title: "Open Settings", style: .cancel) { alert in
            if let appSettingsURL = NSURL(string: UIApplication.openSettingsURLString) {
                UIApplication.shared.open(appSettingsURL as URL, options: [:], completionHandler: nil)
            }
        })
        DispatchQueue.main.async {
            VariableInfo.windows?.rootViewController?.present(alert, animated: true, completion: nil)
        }
    }
    
    private func alertPromptToAllowPhotoLibraryAccessViaSettings() {
        let alert = UIAlertController(title: "\(AppInfo.appName)  Would Like To Access the PhotoLigrary", message: "Please grant permission to use the Photo so that you can customer benefit.", preferredStyle: .alert )
        alert.addAction(UIAlertAction(title: "Open Settings", style: .cancel) { alert in
            if let appSettingsURL = NSURL(string: UIApplication.openSettingsURLString) {
                UIApplication.shared.open(appSettingsURL as URL, options: [:], completionHandler: nil)
            }
        })
        DispatchQueue.main.async {
            VariableInfo.windows?.rootViewController?.present(alert, animated: true, completion: nil)
        }
    }
    
}

extension ImagePicker: UIImagePickerControllerDelegate {
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    public func imagePickerController(_ picker: UIImagePickerController,
                                      didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        guard let image = info[.editedImage] as? UIImage else {
            return self.pickerController(picker, didSelect: nil)
        }
        self.pickerController(picker, didSelect: image)
    }
}

extension ImagePicker: UINavigationControllerDelegate {
    
}
