//
//  DBManager.swift
//
//  Created by Vishal on 09/11/20.
//  Copyright © 2020 VISHAL. All rights reserved.
//

import Foundation
import UIKit
import FMDB
class DBManager: NSObject {
    static let shared: DBManager = DBManager()
    
    let databaseFileName = "*.db"
    var pathToDatabase: String!
    var database: FMDatabase!
    
    let field_user_id = "user_id"
    let field_user_name = "user_name"
    let field_user_email = "user_email"
    let field_user_telephone = "user_telephone"
    let field_user_society = "user_society"
    let field_storage_space_limit = "storage_space_limit"
    
    let recode_id = "recode_id"
    let recode_name = "recode_name"
    let recode_path = "recode_path"
    let recode_date = "recode_date"
    let recode_uploaded = "recode_uploaded"
    
    override init() {
        super.init()
        
        let documentsDirectory = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString) as String
        pathToDatabase = documentsDirectory.appending("/\(databaseFileName)")
    }
    
    func createDatabase() -> Bool {
        var created = false
        if !FileManager.default.fileExists(atPath: pathToDatabase) {
            database = FMDatabase(path: pathToDatabase!)
            if database != nil {
                // Open the database.
                if database.open() {
                    let userTableQuery = "CREATE TABLE user_Master (\(field_user_id) text not null ,\(field_user_name) text not null , \(field_user_email) text not null , \(field_user_society) text not null , \(field_user_telephone) text not null , \(field_storage_space_limit) double not null)"
                    
                    do {
                        try database.executeUpdate(userTableQuery, values: nil)
                        created = true
                    }
                    catch {
                        showErrorMessage(with: error.localizedDescription)
                    }
                    
                    let eventsTableQuery = "CREATE TABLE recode_Master (\(recode_id) text no null, \(recode_name) text not null, \(recode_path) text not null,\(recode_date) date not null,\(recode_uploaded) integer not null)"
                    do {
                        try database.executeQuery(eventsTableQuery, values: nil)
                        _ = database.executeUpdate(eventsTableQuery, withArgumentsIn: ["?,?,?,?,?"])
                        created = true
                    }
                    catch{
                        showErrorMessage(with: error.localizedDescription)
                    }
                    database.close()
                }
                else {
                    showErrorMessage(with: "Unable to Open Database!")
                }
            }
        }
        return created
    }
    
    func openDatabase() -> Bool {
        if database == nil {
            if FileManager.default.fileExists(atPath: pathToDatabase) {
                database = FMDatabase(path: pathToDatabase)
            }
        }
        if database != nil {
            if database.open() {
                return true
            }
        }
        return false
    }
    
//    func getUserCount(userData:User) -> Int{
//        var Count: Int = Int()
//        if openDatabase() {
//            let searchSubEvents = "select * from user_Master WHERE user_name LIKE '\(userData.user_name)' AND user_email LIKE '\(userData.user_email)'"
//            //            Count = database.int(forQuery: searchSubEvents)
//            do {
//                let results = try database.executeQuery(searchSubEvents, values: nil)
//                if results.string(forColumn: userData.user_email) != "" {
//                    Count = 1
//                }
//                else{
//                    Count = 0
//                }
//            }
//            catch{
//                Count = 0
//            }
//        }
//        else {
//            showMessage("Unable to Open Database!")
//            return -1
//        }
//        database.close()
//        return Count
//    }
    
//    func insertUserData(userData:User){
//        if openDatabase() {
//            if userData.user_id  == nil || (userData.user_id == "") {
//                userData.user_id = UUID().uuidString
//                UserDefaults.standard.set(userData.user_id, forKey: "UserID")
//            }
//            do {
//                try database.executeUpdate("INSERT INTO user_Master (\(field_user_id), \(field_user_name), \(field_user_email), \(field_user_society), \(field_user_telephone), \(field_storage_space_limit)) VALUES (?, ?, ? , ? , ? ,?)", values: [userData.user_id, userData.user_name, userData.user_email, userData.user_society, userData.user_telephone, userData.storage_space_limit])
//            }
//            catch{
//                print("Insert Error:",error.localizedDescription)
//            }
//            database.close()
//        }
//        else{
//            showMessage("Unable to Open Database!")
//        }
//    }
    
//    func getUsersDetails(userID:String) -> User {
//        let userData: User = User()
//        if openDatabase() {
//            let query = "select * from user_Master WHERE user_id=?";
//            do {
//                let results = try database.executeQuery(query, values: [userID])
//                while results.next() {
//                    userData.user_id = results.string(forColumn: field_user_id)
//                    userData.user_email = results.string(forColumn: field_user_email)
//                    userData.user_name = results.string(forColumn: field_user_name)
//                    userData.user_society = results.string(forColumn: field_user_society)
//                    userData.user_telephone = results.string(forColumn: field_user_telephone)
//                    userData.storage_space_limit = results.double(forColumn: field_storage_space_limit)
//
//                    userInfo = userData
//                    UserDefaults.standard.set(results.string(forColumn: field_user_id), forKey: "UserID")
//                }
//            }
//            catch {
//                showMessage(error.localizedDescription)
//            }
//            database.close()
//        }
//        else{
//            showMessage("Unable to Open Database!")
//        }
//        return userData
//    }
    
//    func updateUserDetails(UserDetails: User) {
//        let _: User = User()
//        if openDatabase() {
//            let query = "UPDATE user_Master set user_name=?, user_email=?, user_society=?, user_telephone=?, storage_space_limit=?  WHERE user_id=?"
//            do {
//                try database.executeUpdate(query, values: [UserDetails.user_name, UserDetails.user_email, UserDetails.user_society, UserDetails.user_telephone, UserDetails.storage_space_limit, UserDetails.user_id])
//            }
//            catch {
//                showMessage("Fail to Update Details")
//            }
//        }
//        else {
//            showMessage("Unable to Open Database!")
//        }
//        database.close()
//    }
    
    // Recording List
//    func getRecordList(){
//        count = 0
//        arrOfVoiceMemos.removeAll()
//        if openDatabase(){
//            let query = "SELECT * FROM recode_Master ORDER BY recode_date DESC "
//            do {
//                let results = try database.executeQuery(query, values: nil)
//                while results.next() {
//                    let recordData:Record = Record()
//                    recordData.recode_id = results.string(forColumn: recode_id)
//                    recordData.recode_name = results.string(forColumn: recode_name)
//                    recordData.recode_date = results.string(forColumn: recode_date)
//                    recordData.recode_path = results.string(forColumn: recode_path)
//                    recordData.recode_uploaded = results.int(forColumn: recode_uploaded)
//                    count = count + 1
//                    arrOfVoiceMemos.append(recordData)
//                }
//            }
//            catch{
//                print(error.localizedDescription)
//            }
//            database.close()
//        }
//        else{
//            showMessage("Unable to Open Database!")
//        }
//    }
    
    // Recording List
//    func getrandomcount() {
//        count = 0
//        arrOfVoiceMemos.removeAll()
//        arrOfCommonName = []
//        let _:Record = Record()
//        if openDatabase(){
//            let query = "SELECT recode_name FROM recode_Master WHERE recode_name LIKE 'NewRecording%' "
//            do {
//                let results = try database.executeQuery(query, values: nil)
//                while results.next() {
//                    var value = ""
//                    value = results.string(forColumn: recode_name)!
//                    arrOfCommonName.append(value)
//                }
//            }
//            catch{
//                showMessage(error.localizedDescription)
//            }
//            database.close()
//        }
//        else{
//            showMessage("Unable to Open Database!")
//        }
//    }
    
//    func insertRecording(recordData:Record){
//        if openDatabase() {
//            if recordData.recode_id  == nil || (recordData.recode_id == "") {
//                recordData.recode_id = UUID().uuidString
//            }
//            let date = Date()
//            _ =  database.executeUpdate("INSERT INTO recode_Master (\(recode_id), \(recode_name), \(recode_path), \(recode_date), \(recode_uploaded)) VALUES (?, ?, ? , ? , ?)", withArgumentsIn: [recordData.recode_id! , recordData.recode_name! , recordData.recode_path! ,date , recordData.recode_uploaded!])
//            database.close()
//        }
//        else{
//            showMessage("Unable to Open Database!")
//        }
//    }
    
//    func updateRecording(recordData:Record , oldname:String){
//        let _: User = User()
//        if recordData.recode_id  == nil || (recordData.recode_id == "") {
//            recordData.recode_id = UUID().uuidString
//        }
//        if openDatabase() {
//            let date = Date()
//            let query = "UPDATE recode_Master set \(recode_id)=?, \(recode_name)=?, \(recode_path)=?, \(recode_date)=?, \(recode_uploaded)=?  WHERE \(recode_name)=?"
//            do {
//                try database.executeUpdate(query, values: [recordData.recode_id!, recordData.recode_name!, recordData.recode_path!, date, recordData.recode_uploaded!, oldname])
//            }
//            catch {
//                showMessage("Fail to update recoding Details")
//            }
//        }
//        else {
//            showMessage("Unable to Open Database!")
//        }
//        database.close()
//    }
    
//    func editFileName(recordData:Record , oldFilename:String){
//        let _: User = User()
//        if recordData.recode_id  == nil || (recordData.recode_id == "") {
//            recordData.recode_id = UUID().uuidString
//        }
//        if openDatabase() {
//            let query = "UPDATE recode_Master set \(recode_id)=?, \(recode_name)=?, \(recode_path)=?, \(recode_date)=?, \(recode_uploaded)=?  WHERE \(recode_name)=?"
//            do {
//                try database.executeUpdate(query, values: [recordData.recode_id, recordData.recode_name, recordData.recode_path, recordData.recode_date, recordData.recode_uploaded!, oldFilename])
//            }
//            catch {
//                showMessage("Fail to update recoding Details")
//            }
//        }
//        else {
//            showMessage("Unable to Open Database!")
//        }
//        database.close()
//    }
    
//    func deleteRecoding(recodeID:String){
//        if openDatabase(){
//            do {
//                try database.executeUpdate("DELETE from recode_Master WHERE \(recode_id)=?", values: [recodeID])
//            }
//            catch{
//                showMessage("Fail to delete Recoding")
//            }
//        }
//        else{
//            showMessage("Unable to Open Database!")
//        }
//    }
}
