//
//  SceneDelegate.swift
//  ProjectSetup
//
//  Created by Nexios on 03/08/21.
//

import UIKit
import Firebase
import FirebaseRemoteConfig
class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    var remoteConfig: RemoteConfig!
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
        guard let _ = (scene as? UIWindowScene) else { return }
        
        configCall()
    }
    
    func configCall(){
        remoteConfig = RemoteConfig.remoteConfig()
        let settings = RemoteConfigSettings()
        settings.minimumFetchInterval = 0
        remoteConfig.configSettings = settings
        if Utility.checkInternet() {
            fetchConfig()
        } else {
            SVProgressHUD.showError(withStatus: "Please check internet connection and retry.")
        }
    }
    
    func fetchConfig() {
        let expirationDuration = 0
        remoteConfig.fetch(withExpirationDuration: TimeInterval(expirationDuration)) { (status, error) -> Void in
            if status == .success {
                self.remoteConfig.activate { (success, error) in
                    if success{
                        let isLive = RemoteConfig.remoteConfig()
                            .configValue(forKey: "isLive")
                            .stringValue ?? ""
                        AdsInfo.isLiveAds = isLive == "false" ? false : true
                        if AdsInfo.isLiveAds {
                            AdsInfo.openApp_Ads = RemoteConfig.remoteConfig()
                                .configValue(forKey: "openApp_Ads")
                                .stringValue ?? ""
                            
                            AdsInfo.interstitial_Ads = RemoteConfig.remoteConfig()
                                .configValue(forKey: "interstitial_Ads")
                                .stringValue ?? ""
                            
                            AdsInfo.banner_Ads = RemoteConfig.remoteConfig()
                                .configValue(forKey: "banner_Ads")
                                .stringValue ?? ""
                            
                            AdsInfo.native_Ads = RemoteConfig.remoteConfig()
                                .configValue(forKey: "native_Ads")
                                .stringValue ?? ""
                            
                            let randomAds = RemoteConfig.remoteConfig()
                                .configValue(forKey: "isAdsRandom")
                                .stringValue ?? "0"
                            AdsInfo.showAdsCount = Int(randomAds) ?? 0
                            
                            AdsManager.shared.createAndLoadInterstitial()
                            
                        } else {
                            AdsInfo.interstitial_Ads = ""
                            AdsInfo.banner_Ads = ""
                            AdsInfo.native_Ads = ""
                            AdsInfo.openApp_Ads = ""
                            AdsInfo.showAdsCount = 3
                        }
                    }
                }
            }
        }
    }

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not necessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        AdsManager.shared.tryToPresentAd()
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.

        // Save changes in the application's managed object context when the application transitions to the background.
        (UIApplication.shared.delegate as? AppDelegate)?.saveContext()
    }


}

